package com.citi.training.employee.menu;

import java.util.Scanner;

/**
 * This class contains some utilities to get user input
 * using the Scanner class.
 */
public class CmdLineScanner implements InputScanner {

    // Create a Scanner object, to get keyboard input.
    private Scanner scanner = new Scanner(System.in);

    // Get a String from the user.
    public String getString(String promptMsg) {
        System.out.printf("%s", promptMsg);
        return scanner.next();
    }

    // Get a double from the user.
    public double getDouble(String promptMsg) {
        System.out.printf("%s", promptMsg);
        return scanner.nextDouble();

    }

    // Get an int from the user.
    public int getInt(String promptMsg) {
        System.out.printf("%s", promptMsg);
        return scanner.nextInt();
    }
    
    public void clearScanner() {
    	if(scanner.hasNextLine()) {
    		scanner.nextLine();
    	}
    }
}
