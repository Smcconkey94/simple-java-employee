package com.citi.training.employee.menu;

import java.util.HashMap;

import com.citi.training.employee.model.Employee;

public class Main {

    public static void main(String[] args) {

        InputScanner inputScanner = new CmdLineScanner();
        int id;
        double amount;
        Employee employee;
        HashMap<Integer, Employee> allEmployees = new HashMap<Integer, Employee>();

        // Display menu options in a loop.
        int option = -1;
        do {
            System.out.println();
            System.out.println("---------------------------------------------------------");
            System.out.println("1. Hire employee");
            System.out.println("2. Fire employee");
            System.out.println("3. Give employee a pay rise");
            System.out.println("4. Get an employee");
            System.out.println("5. Get all employees");
            System.out.println("6. Quit");
            
            try {
            	option = inputScanner.getInt("\nEnter option => ");
                
                switch (option) {

                    case 1:
                        employee = new Employee(inputScanner.getString("\nEnter employee name: "));
                        allEmployees.put(employee.getId(), employee);
                        System.out.println("Hired: " + employee + "\n");
                        break;
        
                    case 2:
                        id = inputScanner.getInt("Enter id: ");
                        allEmployees.remove(id);
                        System.out.println("Fired employee with id " + id + "\n");
                        break;
        
                    case 3:
                        id = inputScanner.getInt("Enter id: ");
                        amount = inputScanner.getDouble("Enter dollar amount: ");
                        employee = allEmployees.get(id);
                        if (employee != null) {
                            employee.payRaise(amount);
                            System.out.printf("Given $%.2f pay rise to employee:%s\n", amount, employee);
                        } else {
                            System.out.printf("No employee with id %d.\n", id);
                        }
                        break;
        
                    case 4:
                        id = inputScanner.getInt("Enter id: ");
                        employee = allEmployees.get(id);
                        if (employee != null) {
                            System.out.printf("Employee details: %s.\n", employee);
                        } else {
                            System.out.printf("No employee with id %d.\n", id);
                        }
                        break;
        
                    case 5:
                        System.out.println("All employees");
                        displayEmployeeMap(allEmployees);
                        break;
        
                    case 6:
                        // This is a valid option, but there's nothing to do here.
                        break;
        
                    default:
                        System.out.println("Invalid option selected.");
                }
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("Please enter a valid number i.e: 1-6");
			} finally {inputScanner.clearScanner();}
            
        } while (option != 6);
    }

    // Utility function to display a Map of Integer to Employee.
    public static void displayEmployeeMap(HashMap<Integer, Employee> map) {
        for (Employee emp : map.values()) {
            System.out.println(emp);
        }
    }
}
