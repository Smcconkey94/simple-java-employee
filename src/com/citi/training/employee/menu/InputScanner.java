package com.citi.training.employee.menu;

public interface InputScanner {
	
	String getString(String promptMsg);
	
	double getDouble(String promptMsg);
	
	int getInt(String promptMsg);
	
	void clearScanner();
}
